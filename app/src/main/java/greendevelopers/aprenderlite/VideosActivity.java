package greendevelopers.aprenderlite;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideosActivity extends YouTubeBaseActivity {

    YouTubePlayerView youTubePlayerView;
    Button button;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    private String videoUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        button = (Button) findViewById(R.id.button2);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);

        int course = CourseMenuActivity.course;
        System.out.println("video"+course);
        TextView textView=(TextView) findViewById(R.id.textView2);
        switch (course){
            case 1:
                textView.setText("ANDROID Tutorials");
                videoUrl = "-igAiudpBng";
                break;
            case 2:
                textView.setText("DATABASE Tutorials");
                videoUrl = "FR4QIeZaPeM";
                break;
            case 3:
                textView.setText("HTML Tutorials");
                videoUrl = "-USAeFpVf_A";
                break;
            case 4:
                textView.setText("JAVA Tutorials");
                videoUrl = "IOfF5gg5P8A";
                break;
        }

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(videoUrl);

            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayerView.initialize(PlayerConfig.API_KEY, onInitializedListener);

            }
        });
    }

    public void onClick(View v){
        finish();
    }
}
