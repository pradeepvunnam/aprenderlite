package greendevelopers.aprenderlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CourseTest extends AppCompatActivity {

    private QuestionLibrary mQuestionLibrary = new QuestionLibrary();

    private TextView mScoreView;
    private TextView mQuestionView;
    private Button mButtonChoice1;
    private Button mButtonChoice2;
    private Button mButtonChoice3;

    private String mAnswer;
    private int mScore = 0;
    private int mQuestionNumber = 0;
    private static int noOfQues = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_test);

        noOfQues = 0;
        int course = CourseMenuActivity.course;
        switch (course){
            case 1:
                mQuestionNumber = 0;
                break;
            case 2:
                mQuestionNumber = 3;
                break;
            case 3:
                mQuestionNumber = 6;
                break;
            case 4:
                mQuestionNumber = 9;
                break;
        }
        mScoreView = (TextView)findViewById(R.id.score);
        mQuestionView = (TextView)findViewById(R.id.question);
        mButtonChoice1 = (Button)findViewById(R.id.choice1);
        mButtonChoice2 = (Button)findViewById(R.id.choice2);
        mButtonChoice3 = (Button)findViewById(R.id.choice3);

        updateQuestion();

        final Score score = new Score(0,0);


        //Start of Button Listener for Button1
        mButtonChoice1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (mButtonChoice1.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    score.addCorrect();
                    Toast.makeText(CourseTest.this, "correct", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CourseTest.this, "wrong", Toast.LENGTH_SHORT).show();
                    updateQuestion();
                    score.addWrong();
                }
            }
        });

        //End of Button Listener for Button1

        //Start of Button Listener for Button2
        mButtonChoice2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //My logic for Button goes in here

                if (mButtonChoice2.getText() == mAnswer){
                    mScore = mScore + 1;
                    score.addCorrect();
                    updateScore(mScore);
                    updateQuestion();
                    //This line of code is optiona
                    Toast.makeText(CourseTest.this, "correct", Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(CourseTest.this, "wrong", Toast.LENGTH_SHORT).show();
                    score.addWrong();
                    updateQuestion();
                }
            }
        });

        //End of Button Listener for Button2


        //Start of Button Listener for Button3
        mButtonChoice3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //My logic for Button goes in here

                if (mButtonChoice3.getText() == mAnswer){
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    score.addCorrect();
                    //This line of code is optiona
                    Toast.makeText(CourseTest.this, "correct", Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(CourseTest.this, "wrong", Toast.LENGTH_SHORT).show();
                    updateQuestion();
                    score.addWrong();
                }
            }
        });

    }

    private void updateQuestion(){
        mQuestionView.setText(mQuestionLibrary.getQuestion(mQuestionNumber));
        mButtonChoice1.setText(mQuestionLibrary.getChoice1(mQuestionNumber));
        mButtonChoice2.setText(mQuestionLibrary.getChoice2(mQuestionNumber));
        mButtonChoice3.setText(mQuestionLibrary.getChoice3(mQuestionNumber));

        mAnswer = mQuestionLibrary.getCorrectAnswer(mQuestionNumber);
        if(noOfQues<3) {
            mQuestionNumber++;
            noOfQues++;
        }else{
            mQuestionView.setText("Test Completed. Please click quit to go back.");
            mButtonChoice1.setVisibility(View.GONE);
            mButtonChoice2.setVisibility(View.GONE);
            mButtonChoice3.setVisibility(View.GONE);
        }
    }

    private void updateScore(int point) {
        mScoreView.setText("" + mScore);
    }

    public void quitTest(View v){
        finish();
    }
}