package greendevelopers.aprenderlite.Login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by s528182 on 4/24/2017.
 */

public class Id {
    @SerializedName("$old")
    private String old;

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }
}
