package greendevelopers.aprenderlite;

import greendevelopers.aprenderlite.Login.User;

/**
 * Created by s528182 on 4/24/2017.
 */

public class Common {
    private static String DB_NAME = "aprenderlite";
    private static String COLLECTION_NAME = "user";
    public static  String API_KEY = "3LZx8zpU75UbvOX1KI14CvhEnM-tExFS";


    public static String getAddressSingle(User user){
        String baseUrl = String.format("https://api.mlab.com/api/1/databases/%s/collections/%s",DB_NAME,COLLECTION_NAME);
        StringBuilder stringBuilder = new StringBuilder(baseUrl);
        stringBuilder.append("/"+user.get_id().getOld()+"?apiKey="+API_KEY);
        return stringBuilder.toString();
    }

    public static String getAddressAPI(){
        String baseUrl = String.format("https://api.mlab.com/api/1/databases/%s/collections/%s",DB_NAME,COLLECTION_NAME);
        StringBuilder stringBuilder = new StringBuilder(baseUrl);
        stringBuilder.append("?apiKey="+API_KEY);
        return stringBuilder.toString();
    }
}
