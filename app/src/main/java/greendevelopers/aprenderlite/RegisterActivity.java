package greendevelopers.aprenderlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    public static String email, pswd;
    Button bRegister;
    EditText etEmail, etPassword, etName,etConfPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void Register(View v) {
        bRegister = (Button) findViewById(R.id.bRegister);
        etEmail = (EditText) findViewById(R.id.etEmailId);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfPassword = (EditText) findViewById(R.id.etConfirmPswd);
        etName = (EditText) findViewById(R.id.etName);

        System.out.println(etEmail.getText().toString().trim());
        System.out.println(etPassword.getText().toString().trim());
        System.out.println(etConfPassword.getText().toString().trim());
        System.out.println(etName.getText().toString().trim());


        if(etEmail.getText().toString().trim()!="" && etName.getText().toString().trim()!="" &&
           etPassword.getText().toString().trim()!="" && etConfPassword.getText().toString().trim()!="" &&
           etPassword.getText().toString().trim().equals(etConfPassword.getText().toString().trim()))
        {
            email = etEmail.getText().toString();
            pswd = etPassword.getText().toString();
            Toast.makeText(RegisterActivity.this, "Success", Toast.LENGTH_SHORT).show();
            finish();
        }else{
            Toast.makeText(RegisterActivity.this, "Failure", Toast.LENGTH_SHORT).show();
        }

        //finish();
    }
}
