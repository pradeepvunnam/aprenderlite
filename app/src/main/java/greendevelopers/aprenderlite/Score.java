package greendevelopers.aprenderlite;

/**
 * Created by S528182 on 4/25/2017.
 */

public class Score {
    public static int correct;
    public static int wrong;

    public Score(int correct, int wrong) {
        this.correct = correct;
        this.wrong = wrong;
    }

    public void addCorrect(){
        System.out.println("correct added");
        this.correct +=1;
    }

    public void addWrong(){
        System.out.println("Wrong added");
        this.wrong +=1;
    }
}
