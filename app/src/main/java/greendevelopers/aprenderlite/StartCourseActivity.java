package greendevelopers.aprenderlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class StartCourseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_course);

        int course = CourseMenuActivity.course;
        TextView textView=(TextView) findViewById(R.id.textView5);
        TextView tv1=(TextView) findViewById(R.id.tv1);
        TextView tv2=(TextView) findViewById(R.id.tv2);
        TextView tv3=(TextView) findViewById(R.id.tv3);
        TextView tv4=(TextView) findViewById(R.id.tv4);
        switch (course){
            case 1:
                textView.setText("ANDROID");
                tv1.setText("Android is an open source and Linux-based operating system for mobile devices such as smartphones and tablet computers.");
                tv2.setText("Android offers a unified approach to application development for mobile devices.");
                tv3.setText("Android applications are usually developed in the Java language using the Android Software Development Kit.");
                tv4.setText("Android powers hundreds of millions of mobile devices in more than 190 countries around the world.");
                break;
            case 2:
                textView.setText("DATABASE");
                tv1.setText("SQL stands for Structured Query Language and lets you access and manipulate databases.");
                tv2.setText("SQL is a standard language for accessing and manipulating databases.");
                tv3.setText("RDBMS stands for Relational Database Management System.");
                tv4.setText("RDBMS is the basis for SQL, and for all modern database systems such as MS SQL Server, IBM DB2, Oracle, MySQL, and Microsoft Access.");
                break;
            case 3:
                textView.setText("HTML");
                tv1.setText("HTML is the standard markup language for creating Web pages. HTML stands for Hyper Text Markup Language. ");
                tv2.setText("HTML describes the structure of Web pages using markup.");
                tv3.setText("\n" +
                        "HTML tags are element names surrounded by angle brackets and these tags normally come in pairs. \n" +
                        "The first tag in a pair is the start tag, the second tag is the end tag.");
                tv4.setText("The DOCTYPE declaration represents the document type and helps browsers to display web pages correctly.");

                break;
            case 4:
                textView.setText("JAVA");
                break;
        }
    }

    public void backToMain(View view){
        finish();
    }
}
