package greendevelopers.aprenderlite;

import android.widget.TextView;

import static greendevelopers.aprenderlite.R.id.textView;

/**
 * Created by s528182 on 4/17/2017.
 */

public class QuestionLibrary {

    private String mQuestions [] = {
            "Which of the following is/are the subclasses in Android?",
            "What is an anonymous class in Android?",
            "Android is developed in?",
            "In SQL, to combine rows from two queries eliminating duplicates we use",
            "Data warehouses are built from operational databases using which tools",
            "The SQL cache stores the most recently executed SQL statements or procedures?",
            "Which of the following tags is used for hyperlink?",
            "The HTML tag for a new paragraph is ",
            "JavaScript is executed by",
            "What is the default value of float variable?",
            "Method Overriding is an example of",
            "Which of the following that a method does not return a value",
            "Dummy Question"
    };

    private String mChoices [][] = {
            {"Action bar activity", "Preference activity", "All the above"},
            {"Interface class", "Class with no name", "Manifest file"},
            {"Sun Microsystems", "Oracle Corporation", "Oracle and Sun Microsystems"},
            {"Inner or outer Join", "union", "unionAll"},
            {"ETL", "OLAP", "Data Analysis"},
            {"True", "False", "Depends on SQL"},
            {"a", "li", "link"},
            {"p", "pg", "para"},
            {"Web service", "Java compiler", "Web browser"},
            {"0.0d", "0.0f", "0"},
            {"Static Binding", "Dynamic Binding", "Both of the above"},
            {"Static", "Private", "Void"},
            {"Linux", "Unix", "Windows"}
    };

    private String mCorrectAnswers[] = {"All the above","Class with no name","Oracle Corporation","union","ETL","True","a","p","Web browser","0.0f", "Dynamic Binding", "Void", "Stem"};

    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }


    public String getChoice1(int a) {
        String choice0 = mChoices[a][0];
        return choice0;
    }


    public String getChoice2(int a) {
        String choice1 = mChoices[a][1];
        return choice1;
    }

    public String getChoice3(int a) {
        String choice2 = mChoices[a][2];
        return choice2;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }

}
